const arr = [1,2,3,4,5,6,7,3,4,5,7,3,2,3,5,6,8];
const itemsInBlock = 8;
let visibleBlocks = 0;

function getHeight(){
    let sum = 0;
    visibleBlocks = visibleBlocks + 1;
    let visibleItems = visibleBlocks * itemsInBlock;

    if (visibleItems > arr.length) {
        visibleItems = arr.length;
        // call function that remove "more button"
    }
    for(let i=0; i<visibleItems; i++) {
        sum = sum + arr[i];
    }

    return sum;
}

getHeight();
getHeight();
getHeight();
getHeight();
getHeight();
getHeight();